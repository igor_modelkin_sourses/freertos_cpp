/**************************** (C) __________ LLC *******************************
 * @module  Queue_T
 * @file    Queue_T.cpp
 * @version 1.0.0
 * @date    XX.XX.XXXX
 * $brief   Implementation of the MODULE functionality.
 *          [...]
 *******************************************************************************
 * @history     Version  Author      Comment
 * XX.XX.XXXX   1.0.0    XXX         First release.
 *******************************************************************************
 */



/* Includes ------------------------------------------------------------------*/

#include "Queue_T.h"

/* Verification of the imported configuration parameters ---------------------*/

/* Definitions of public methods ---------------------------------------------*/

uint16_t Queue::API_Get_Total_heap(void){
  if( this->pQueue_handle != 0 ) return this->pQueue_total_heap;
  return 0;
}
uint16_t Queue::API_Get_Mess_Free(void){
  if( this->pQueue_handle != 0 ) return uxQueueSpacesAvailable( this->pQueue_handle ) ;
  return 0;
}
uint16_t Queue::API_Get_Mess_Cnt(void){
  if( this->pQueue_handle != 0 ) return (this->pQueue_Length - uxQueueSpacesAvailable( this->pQueue_handle )) ;
  return 0;
}
uint16_t Queue::API_Get_Mess_Nrb(void){
  if( this->pQueue_handle != 0 ) return this->pQueue_Length;
  return 0;
}  
uint16_t Queue::API_Get_Mess_Size(void){
  if( this->pQueue_handle != 0 ) return this->pQueue_Size;
  return 0;
}  

uint16_t Queue::API_MessWait(void){//���������� ���-�� ��������� ������
  if( this->pQueue_handle != 0 )
  {
    return uxQueueMessagesWaiting( this->pQueue_handle );
  }
  return 0;
}
uint16_t Queue::API_Receive(void * pvBuffer, uint32_t xTicksToWait){//1 ���� ������� ����� ���� ����� 0
  if( this->pQueue_handle != 0 )
  {
    return (uint16_t) xQueueReceive( this->pQueue_handle, pvBuffer, xTicksToWait ); //������� ����
  }
  return 0;
}
uint16_t Queue::API_Peek(void * pvBuffer, uint32_t xTicksToWait){//1 ���� ������� ���������� � ���� ����� 0
  if( this->pQueue_handle != 0 )
  {
    return (uint16_t)xQueuePeek( this->pQueue_handle, pvBuffer, xTicksToWait ); //������� ����
  }
  return 0;
}
uint16_t Queue::API_SendToBack(void * pvBuffer, uint32_t xTicksToWait){//1 ���� ������� �������� ���� ����� 0
  if( this->pQueue_handle != 0 ) //������ ������ FIFO
  {
    return (uint16_t) xQueueSendToBack( this->pQueue_handle, pvBuffer, xTicksToWait ); //������� ����
  }
  return 0;
}
uint16_t Queue::API_SendToFront(void * pvBuffer, uint32_t xTicksToWait){//1 ���� ������� �������� ���� ����� 0
  if( this->pQueue_handle != 0 ) //������ ������ LIFO
  {
    return (uint16_t) xQueueSendToBack( this->pQueue_handle, pvBuffer, xTicksToWait ); //������� ����
  }
  return 0;
}
uint16_t Queue::API_Overwrite(void * pvBuffer){//������ 1
  if( this->pQueue_handle != 0 )
  {
    xQueueOverwrite( this->pQueue_handle, pvBuffer); //������� ����
    if( this->pQueue_handle != 0 ) return this->pQueue_Size;
  }
  return 0;
}


uint16_t Queue::API_Get_Mess_CntISR(void){
  if( this->pQueue_handle != 0 ) return uxQueueMessagesWaitingFromISR( this->pQueue_handle ) ;
  return 0;
}
uint16_t Queue::API_Get_Mess_FreeISR(void){
  if( this->pQueue_handle != 0 ) return (this->pQueue_Length - uxQueueMessagesWaitingFromISR( this->pQueue_handle )) ;
  return 0;
}
uint16_t Queue::API_Get_Mess_NrbISR(void){
  if( this->pQueue_handle != 0 ) return this->pQueue_Length;
  return 0;
}  
uint16_t Queue::API_Get_Mess_SizeISR(void){
  if( this->pQueue_handle != 0 ) return this->pQueue_Size;
  return 0;
}  

uint16_t Queue::API_MessWaitISR(void){//���������� ���-�� ��������� ������
  if( this->pQueue_handle != 0 )
  {
    return uxQueueMessagesWaitingFromISR( this->pQueue_handle );
  }
  return 0;
}
uint16_t Queue::API_ReceiveISR(void * pvBuffer, uint32_t ForceTaskYIELD){//1 ���� ������� ����� ���� ����� 0
  if( this->pQueue_handle != 0 )
  {
    if ( ForceTaskYIELD != 0 ) ForceTaskYIELD = 1;
    return (uint16_t) xQueueReceiveFromISR( this->pQueue_handle, pvBuffer, (signed portBASE_TYPE*) &ForceTaskYIELD);
  }
  return 0;
}
uint16_t Queue::API_PeekISR(void * pvBuffer){//1 ���� ������� ���������� � ���� ����� 0
  if( this->pQueue_handle != 0 )
  {
    return (uint16_t) xQueuePeekFromISR( this->pQueue_handle, pvBuffer ); //���������� � ����
  }
  return 0;
}
uint16_t Queue::API_SendToBackISR(void * pvBuffer, uint32_t ForceTaskYIELD){//1 ���� ������� �������� ���� ����� 0
  if( this->pQueue_handle != 0 )//������ ������ FIFO
  {
    if ( ForceTaskYIELD != 0 ) ForceTaskYIELD = 1;
    return (uint16_t) xQueueSendToBackFromISR( this->pQueue_handle, pvBuffer, (signed portBASE_TYPE*) &ForceTaskYIELD);
  }
  return 0;
}
uint16_t Queue::API_SendToFrontISR(void * pvBuffer, uint32_t ForceTaskYIELD){//1 ���� ������� �������� ���� ����� 0
  if( this->pQueue_handle != 0 )
  {
    if ( ForceTaskYIELD != 0 ) ForceTaskYIELD = 1;
    return (uint16_t) xQueueSendToFrontFromISR( this->pQueue_handle, pvBuffer, (signed portBASE_TYPE*) &ForceTaskYIELD);
  }
  return 0;
}
uint16_t Queue::API_OverwriteISR(void * pvBuffer, uint32_t ForceTaskYIELD){//1 ���� ������� �������� ���� ����� 0
  if( this->pQueue_handle != 0 )
  {
    if ( ForceTaskYIELD != 0 ) ForceTaskYIELD = 1;
    return (uint16_t) xQueueOverwriteFromISR( this->pQueue_handle, pvBuffer, (signed portBASE_TYPE*) &ForceTaskYIELD);
  }
  return 0;
}


/* Definitions of private methods --------------------------------------------*/

/* Definitions of task body --------------------------------------------------*/

/* Definitions of task costructor/destructor ---------------------------------*/

void* Queue::operator new (size_t size){
  return pvPortMalloc (size);
}
void Queue::operator delete (void* Ptr){
  vPortFree (Ptr);
}

Queue::Queue(uint16_t Length, uint16_t Size){
  this->pQueue_handle = xQueueCreate ( Length, Size );
  
  if (this->pQueue_handle == 0) this->~Queue();  
  
  uint32_t* Head = (uint32_t*) this->pQueue_handle;
  uint32_t* Tall = (uint32_t*) ((uint32_t)this->pQueue_handle + 4);
  
  this->pQueue_total_heap = (uint32_t)(*Tall - *Head + 76);
  
  this->pQueue_Length   = Length;
  this->pQueue_Size     = Size; 
}
Queue::~Queue(){
  if (this->pQueue_handle != 0) vQueueDelete(this->pQueue_handle);
}


/*************************** (C) __________ LLC ************** end of file ****/