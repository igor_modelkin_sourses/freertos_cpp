
/**************************** (C) __________ LLC *******************************
* @module  BaseClass
* @file    BaseClass.h
* @version 1.0.0
* @date    XX.XX.XXXX
* $brief   Interface of the MODULE module.
*          [...]
*******************************************************************************
* @history     Version  Author      Comment
* XX.XX.XXXX   1.0.0    XXX         First release.
*******************************************************************************
*/

/* Define to prevent recursive  ----------------------------------------------*/
#ifndef BASECLASS_H
#define BASECLASS_H

/* Includes ------------------------------------------------------------------*/
#include "stdint.h"
#include "FreeRTOS.h"

/* Definitions of public constants -------------------------------------------*/   
/* Declarations of public data types -----------------------------------------*/
/* Declarations of public variables ------------------------------------------*/ 
/* Declarations of global C++ Data -------------------------------------------*/
#ifdef __cplusplus
/* Includes ------------------------------------------------------------------*/
/* Declarations of global (public) constants ---------------------------------*/
/* Declarations of global (public) data types --------------------------------*/
/* Declarations of global (public) variables ---------------------------------*/
/* Declarations of global (public) class -------------------------------------*/
class BaseClass{ //: public BaseClass
  
public:
  /* Declarations of public class variables ----------------------------------*/ 
  /* Declarations of public volatile class variables -------------------------*/  
  /* Declarations of public class methods ------------------------------------*/  
  void* operator new    ( size_t size );
  void  operator delete ( void* Ptr );
  uint8_t operator != ( BaseClass  &b );
  uint8_t operator == ( BaseClass  &b );
  
  static uint8_t API_Check_Ptr     (void* adress);
  static uint8_t API_Check_RAM_Ptr (void* adress);
  static uint8_t API_Check_ROM_Ptr (void* adress);
  
  static void*   API_Use_Ptr     (void* adress);
  static void*   API_Use_RAM_Ptr (void* adress);
  static void*   API_Use_ROM_Ptr (void* adress);
  
  static void    API_Falut_Fatch (void);
  
protected:
  /* Declarations of protected class variables -------------------------------*/ 
  /* Declarations of protected volatile class variables ----------------------*/
  /* Declarations of protected methods ---------------------------------------*/ 
  
private:
  /* Declarations of private class variables ---------------------------------*/
  uint32_t pSelfSize;
  
  /* Declarations of private volatile class variables ------------------------*/
  /* Declarations of private methods -----------------------------------------*/
    
public:
  /* Declarations of task costructor/destructor ------------------------------*/
  BaseClass(uint32_t size); //:BaseClass(sizeof ()){};
  ~BaseClass();
  
};
#endif

#endif  /* #ifndef */

