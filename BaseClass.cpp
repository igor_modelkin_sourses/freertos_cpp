
/**************************** (C) __________ LLC *******************************
 * @module  BaseClass
 * @file    BaseClass.cpp
 * @version 1.0.0
 * @date    XX.XX.XXXX
 * $brief   Interface of the MODULE module.
 *          [...]
 *******************************************************************************
 * @history     Version  Author      Comment
 * XX.XX.XXXX   1.0.0    XXX         First release.
 *******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "BaseClass.h"
#include "task.h"
#include "CPU.h"
#include "string.h"

/* Definitions of private constants ------------------------------------------*/
/* Definitions of private data types -----------------------------------------*/
/* Definitions of private variables ------------------------------------------*/ 
/* Definitions of private volatile variables ---------------------------------*/

/* Definitions of public methods ---------------------------------------------*/
void*   BaseClass::operator new (size_t size){
  void* result = pvPortMalloc (size);
  if (result != 0 )memset((char*)API_Use_Ptr(result), 0x00, size);
  return result;
}
void    BaseClass::operator delete (void* Ptr){
  vPortFree (Ptr);  
}
uint8_t BaseClass::operator != ( BaseClass  &b){
  return memcmp((const void*)this, (const void*)&b, pSelfSize);
}
uint8_t BaseClass::operator == ( BaseClass  &b){
  return !memcmp((const void*)this, (const void*)&b, pSelfSize);
}

uint8_t BaseClass::API_Check_Ptr (void* adress){
  return API_CPU_Check_RAM_ROM_Adr((uint32_t) adress);
};
uint8_t BaseClass::API_Check_RAM_Ptr (void* adress){
  return API_CPU_Check_RAM_Adr((uint32_t) adress);
};
uint8_t BaseClass::API_Check_ROM_Ptr (void* adress){
  return API_CPU_Check_ROM_Adr((uint32_t) adress);
};

void*   BaseClass::API_Use_Ptr (void* adress){
  if (API_CPU_Check_RAM_ROM_Adr((uint32_t) adress) == 0)
  {
    API_Falut_Fatch();
  };  
  return adress;
};
void*   BaseClass::API_Use_RAM_Ptr (void* adress){
  if (API_CPU_Check_RAM_Adr((uint32_t) adress) == 0)
  {
    API_Falut_Fatch();
  };  
  return adress;
};
void*   BaseClass::API_Use_ROM_Ptr (void* adress){
  if (API_CPU_Check_ROM_Adr((uint32_t) adress) == 0)
  {
    API_Falut_Fatch();
  };  
  return adress;
};

void    BaseClass::API_Falut_Fatch (void){
  for(;;);
};

/* Definitions of protected methods ------------------------------------------*/
/* Definitions of private methods --------------------------------------------*/

/* Definitions of task costructor/destructor ---------------------------------*/
BaseClass::BaseClass(uint32_t size){
  pSelfSize = size;
}
BaseClass::~BaseClass(){
  
}