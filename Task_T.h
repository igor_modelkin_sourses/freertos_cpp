/**************************** (C) __________ LLC *******************************
 * @module  Control
 * @file    Control.h
 * @version 1.0.0
 * @date    XX.XX.XXXX
 * $brief   Interface of the MODULE module.
 *          [...]
 *******************************************************************************
 * @history     Version  Author      Comment
 * XX.XX.XXXX   1.0.0    XXX         First release.
 *******************************************************************************
 */

/* Define to prevent recursive  ----------------------------------------------*/
#ifndef TASK_T_H
#define TASK_T_H



/* Includes ------------------------------------------------------------------*/
#include "stdint.h"
#include "Queue_T.h"
#include "cmsis_os.h"
#include "BaseClass.h"

//#include "FreeRTOS.h"
//#include "task.h"
//#include "SYS.h"

/* Definitions of global (public) constants ----------------------------------*/
/* Declarations of global (public) data types --------------------------------*/
typedef enum{
  TASK_STARTED = 0,
  TASK_DOWORK,
  TASK_NO_QU_CMD,
  TASK_NO_QU_IOSTREAM,
  
  TASK_STACK_FAIL = 127,
  TASK_HEADR_FAIL,
  TASK_NOT_CREATE
}Task_state_Typedef;
typedef struct{
	volatile portSTACK_TYPE	*pxTopOfStack;		/*< Points to the location of the last item placed on the tasks stack.  THIS MUST BE THE FIRST MEMBER OF THE TCB STRUCT. */

	#if ( portUSING_MPU_WRAPPERS == 1 )
		xMPU_SETTINGS xMPUSettings;				/*< The MPU settings are defined as part of the port layer.  THIS MUST BE THE SECOND MEMBER OF THE TCB STRUCT. */
	#endif

	xListItem				xGenericListItem;	/*< The list that the state list item of a task is reference from denotes the state of that task (Ready, Blocked, Suspended ). */
	xListItem				xEventListItem;		/*< Used to reference a task from an event list. */
	unsigned portBASE_TYPE	uxPriority;			/*< The priority of the task.  0 is the lowest priority. */
	portSTACK_TYPE			*pxStack;			/*< Points to the start of the stack. */
	signed char				pcTaskName[ configMAX_TASK_NAME_LEN ];/*< Descriptive name given to the task when created.  Facilitates debugging only. */

	#if ( portSTACK_GROWTH > 0 )
		portSTACK_TYPE *pxEndOfStack;			/*< Points to the end of the stack on architectures where the stack grows up from low memory. */
	#endif

	#if ( portCRITICAL_NESTING_IN_TCB == 1 )
		unsigned portBASE_TYPE uxCriticalNesting; /*< Holds the critical section nesting depth for ports that do not maintain their own count in the port layer. */
	#endif

	#if ( configUSE_TRACE_FACILITY == 1 )
		unsigned portBASE_TYPE	uxTCBNumber;	/*< Stores a number that increments each time a TCB is created.  It allows debuggers to determine when a task has been deleted and then recreated. */
		unsigned portBASE_TYPE  uxTaskNumber;	/*< Stores a number specifically for use by third party trace code. */
	#endif

	#if ( configUSE_MUTEXES == 1 )
		unsigned portBASE_TYPE uxBasePriority;	/*< The priority last assigned to the task - used by the priority inheritance mechanism. */
	#endif

	#if ( configUSE_APPLICATION_TASK_TAG == 1 )
		pdTASK_HOOK_CODE pxTaskTag;
	#endif

	#if ( configGENERATE_RUN_TIME_STATS == 1 )
		unsigned long ulRunTimeCounter;			/*< Stores the amount of time the task has spent in the Running state. */
	#endif

	#if ( configUSE_NEWLIB_REENTRANT == 1 )
		/* Allocate a Newlib reent structure that is specific to this task.
		Note Newlib support has been included by popular demand, but is not
		used by the FreeRTOS maintainers themselves.  FreeRTOS is not
		responsible for resulting newlib operation.  User must be familiar with
		newlib and must provide system-wide implementations of the necessary
		stubs. Be warned that (at the time of writing) the current newlib design
		implements a system-wide malloc() that must be provided with locks. */
		struct _reent xNewLib_reent;
	#endif

} tskTCB;

/* Declarations of global (public) class -------------------------------------*/

class Task: public BaseClass{//������� ����� ����� ������ ������� ������� ������ � �� ������� ������������
public:
  /* Declarations of public class variables ----------------------------------*/  
  
  /* Declarations of public class methods ------------------------------------*/
  virtual       void          Task_body_init    (){
    
  };
  virtual       void          Task_body_process (){
    
  };
  virtual       void          Task_body_deinit  (){
    
  };
  
  char const*   API_Get_Name            (void);
  osPriority    API_Get_Priority        (void);
  xTaskHandle   API_Get_Handle          (void);
  eTaskState    API_Get_OS_State        (void);
  uint16_t      API_Get_Total_heap      (void);
  uint16_t      API_Get_User_heap       (void);
  uint8_t       API_Get_State           (void); 
  void          API_Task_Suspend        (void);
  void          API_Task_Resume         (void);  
   
  uint32_t      API_Get_Tick            (void);
  uint32_t      API_Get_Tick_Free       (void);//������� ���������� ���������� ����� �� �������
  int32_t       API_Get_Tick_Free_Ov    (void);//������� ���������� ���������� ����� �� �������(� �����������)
  void          API_Set_Tick_Lath       (uint32_t);//������� ����������� ������� ���. ��������
  
  void          API_Set_TickRate        (uint32_t);
  void          API_Set_State           (uint8_t);
  void          API_Set_Priority        (uint32_t);    
  
protected:
  /* Declarations of protected class variables -------------------------------*/ 
  char const*   pTask_name;
  uint32_t      pTask_priority;
  xTaskHandle   pTask_handle;
  uint32_t      pTask_total_heap;
  uint32_t      pTask_user_heap;
  uint32_t      pTask_heap_WaterMark;
  
  uint32_t*     volatile pTask_Glob_cnt_ptr; //��������� �� ����. ������� �����
  uint32_t      pTask_Manual_TickRate;       //������ �������� ��� ���� ��
  uint32_t      pTask_FPS;                   //������� ��������� � ����. ���
  uint32_t      pTask_WkUp_Laten;            //������ ��������������� ������� �����
  uint32_t      pTask_WkUp_Laten_Prv;        //������ ��������������� ������� ����. ��� ���
  
  /* Declarations of volatile protected class variables ----------------------*/ 
  volatile      Task_state_Typedef    pTask_state;
  
private:
  /* Declarations of private class variables ---------------------------------*/ 
  
  uint32_t      pTask_Glob_cnt_Lach;            //������� ����. ������� �����
  uint32_t      pTask_Glob_cnt;                 //������� �������� � ��� ���.
  uint32_t      pTask_User_cnt_Lach;            //������� ��� ��������� ������� �������
  
  void (*Task_body_Ptr)(void *);    //��������� �� � ������� ���� �����.(������������ � �������������� ����������� ������)
  
  /* Declarations of private methods -----------------------------------------*/  
  
                void          Task_body         (void * argument);
  
public:
  /* Declarations of task costructor/destructor ------------------------------*/
  uint32_t      pTask_DelayUntil_Latch;         //������� DelayUntil
  
  Task(void (*Task_body)(void*), char const* TaskName, uint16_t Need_heap);
  Task(char const* TaskName, uint16_t Need_heap);  
  virtual ~Task(); 
  
};


#endif  /* #ifndef */

/*************************** (C) __________ LLC ************** end of file ****/
