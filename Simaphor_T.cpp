/**************************** (C) __________ LLC *******************************
 * @module  Simaphor_T
 * @file    Simaphor_T.cpp
 * @version 1.0.0
 * @date    XX.XX.XXXX
 * $brief   Implementation of the MODULE functionality.
 *          [...]
 *******************************************************************************
 * @history     Version  Author      Comment
 * XX.XX.XXXX   1.0.0    XXX         First release.
 *******************************************************************************
 */



/* Includes ------------------------------------------------------------------*/

#include "Simaphor_T.h"

/* Verification of the imported configuration parameters ---------------------*/

/* Definitions of public methods ---------------------------------------------*/

uint16_t  Semaph::API_Give (void){
  return (uint16_t) xSemaphoreGive ( this->API_Use_RAM_Ptr(this->pSemaphHandle) );
}
uint16_t  Semaph::API_Take (uint32_t xTicksToWait){
  return (uint16_t) xSemaphoreTake ( this->API_Use_RAM_Ptr(this->pSemaphHandle) , xTicksToWait );
}
uint16_t  Semaph::API_GiveISR (uint32_t ForceTaskYIELD){
  return (uint16_t) xSemaphoreGiveFromISR ( this->API_Use_RAM_Ptr(this->pSemaphHandle), (signed long *)&ForceTaskYIELD );
}
uint16_t  Semaph::API_TakeISR (uint32_t ForceTaskYIELD){
  return (uint16_t) xSemaphoreTakeFromISR ( this->API_Use_RAM_Ptr(this->pSemaphHandle), (signed long *)&ForceTaskYIELD );
}

xTaskHandle  Mutex::API_GetMutexHolder ( void ){
  return xSemaphoreGetMutexHolder ( this->API_Use_RAM_Ptr(this->pSemaphHandle) );
}

/* Definitions of private methods --------------------------------------------*/

/* Definitions of task body --------------------------------------------------*/

/* Definitions of task costructor/destructor ---------------------------------*/

/*
void* Semaph::operator new (size_t size){
  return pvPortMalloc (size);
}
void Semaph::operator delete (void* Ptr){
  vPortFree (Ptr);
}
*/

Semaph::Semaph( void ):BaseClass(sizeof (Semaph)){
  this->pSemaphHandle = 0;
  vSemaphoreCreateBinary( this->pSemaphHandle );
  if ( this->API_Check_RAM_Ptr( this->pSemaphHandle ) != 0 ) xSemaphoreGive  ( this->pSemaphHandle );
}
Semaph::Semaph( uint8_t Is_Mutex ):BaseClass(sizeof (Semaph)){
  if (Is_Mutex == 1){
    this->pSemaphHandle = 0;
    this->pSemaphHandle = xSemaphoreCreateMutex();
    if( this->API_Check_RAM_Ptr( this->pSemaphHandle ) != 0 ) xSemaphoreGive ( this->pSemaphHandle );
  }
  else{
    this->pSemaphHandle = 0;
    vSemaphoreCreateBinary( this->pSemaphHandle );
    if ( this->API_Check_RAM_Ptr( this->pSemaphHandle ) != 0 ) xSemaphoreGive  ( this->pSemaphHandle );
  }
}
Semaph::~Semaph( void ){
  if ( this->API_Check_RAM_Ptr( this->pSemaphHandle ) != 0 ) vSemaphoreDelete( this->pSemaphHandle );
  this->pSemaphHandle = 0;
}

Mutex::Mutex( void ) : Semaph(1){  
}
//Mutex::~Mutex( void ){
//  if ( this->API_Check_RAM_Ptr( this->pSemaphHandle ) != 0 ) vSemaphoreDelete( this->pSemaphHandle );
//  this->pSemaphHandle = 0;
//}

/*************************** (C) __________ LLC ************** end of file ****/