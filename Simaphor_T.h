/**************************** (C) __________ LLC *******************************
 * @module  Simaphor_T
 * @file    Simaphor_T.h
 * @version 1.0.0
 * @date    XX.XX.XXXX
 * $brief   Interface of the MODULE module.
 *          [...]
 *******************************************************************************
 * @history     Version  Author      Comment
 * XX.XX.XXXX   1.0.0    XXX         First release.
 *******************************************************************************
 */

/* Define to prevent recursive  ----------------------------------------------*/
#ifndef SIMAPHOR_T_H
#define SIMAPHOR_T_H



/* Includes ------------------------------------------------------------------*/
#include "stdint.h"
#include "cmsis_os.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "BaseClass.h"
#ifdef __cplusplus
/* Definitions of global (public) constants ----------------------------------*/

/* Declarations of global (public) data types --------------------------------*/

/* Declarations of global (public) class -------------------------------------*/

class Semaph : public BaseClass
{  
public:
  /* Declarations of public class variables ----------------------------------*/  
  
  /* Declarations of public class methods ------------------------------------*/
  uint16_t  API_Give        (void);                    //1 ���� ������� ����� 0 ���� ������ �� ��������
  uint16_t  API_Take        (uint32_t xTicksToWait);   //1 ���� ���� �������
  
  uint16_t  API_GiveISR     (uint32_t ForceTaskYIELD); //1 ���� ������� �����
  uint16_t  API_TakeISR     (uint32_t ForceTaskYIELD); //1 ���� ���� �������
  
protected:
  /* Declarations of protected class variables -------------------------------*/ 
  xSemaphoreHandle pSemaphHandle;

  
private:
  /* Declarations of private class variables ---------------------------------*/  

  
  /* Declarations of private methods -----------------------------------------*/
  
public:
  /* Declarations of task costructor/destructor ------------------------------*/
  Semaph        (void);
  Semaph        ( uint8_t Is_Mutex );
  ~Semaph       (void); 
  
  //void* operator new    (size_t size);
  //void  operator delete (void* Ptr);
  
};

class Mutex : public Semaph
{  
public:
  /* Declarations of public class variables ----------------------------------*/  
  
  /* Declarations of public class methods ------------------------------------*/
  xTaskHandle API_GetMutexHolder ( void );
  
  
protected:
  /* Declarations of protected class variables -------------------------------*/ 
  
private:
  /* Declarations of private class variables ---------------------------------*/
  
  
  /* Declarations of private methods -----------------------------------------*/
  
public:
  /* Declarations of task costructor/destructor ------------------------------*/
  Mutex        (void);  
//  ~Mutex       (void);   
};
#endif
#endif  /* #ifndef MODULE_H */

/*************************** (C) __________ LLC ************** end of file ****/
