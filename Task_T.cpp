/**************************** (C) __________ LLC *******************************
 * @module  Task_T
 * @file    Task_T.cpp
 * @version 1.0.0
 * @date    XX.XX.XXXX
 * $brief   Implementation of the MODULE functionality.
 *          [...]
 *******************************************************************************
 * @history     Version  Author      Comment
 * XX.XX.XXXX   1.0.0    XXX         First release.
 *******************************************************************************
 */



/* Includes ------------------------------------------------------------------*/

#include "Task_T.h"
#include "CPU.h"

/* Verification of the imported configuration parameters ---------------------*/

/* Definitions of public methods ---------------------------------------------*/

//Task:
char const*             Task::API_Get_Name(){
  return (char const*)Task::pTask_name;
}
osPriority              Task::API_Get_Priority(){
  return (osPriority)uxTaskPriorityGet( API_Use_Ptr(Task::pTask_handle) );
}
xTaskHandle             Task::API_Get_Handle(){
  return API_Use_Ptr(Task::pTask_handle);
}
eTaskState              Task::API_Get_OS_State(){
  return eTaskGetState( API_Use_Ptr(Task::pTask_handle) );
}
uint16_t                Task::API_Get_Total_heap(){
  return Task::pTask_total_heap;
}
uint16_t                Task::API_Get_User_heap(){
  return Task::pTask_user_heap;
}
uint8_t                 Task::API_Get_State(){
  return (uint8_t) Task::pTask_state;
}

void                    Task::API_Task_Suspend(){
  vTaskSuspend( API_Use_Ptr(Task::pTask_handle) );
}
void                    Task::API_Task_Resume(){
  vTaskResume( API_Use_Ptr(Task::pTask_handle) );
}

uint32_t                Task::API_Get_Tick(){
  return (uint32_t)xTaskGetTickCount();
};
uint32_t                Task::API_Get_Tick_Free(){
  uint32_t result = API_Get_Tick() ;
  if (result >= pTask_User_cnt_Lach) return 0;
  result = (pTask_User_cnt_Lach - result);
  return result;
};
int32_t                 Task::API_Get_Tick_Free_Ov(){
  return ((int32_t)pTask_User_cnt_Lach - API_Get_Tick());
};
void                    Task::API_Set_Tick_Lath(uint32_t Max_Tick){
  pTask_User_cnt_Lach = API_Get_Tick() + Max_Tick;
};


void                    Task::API_Set_TickRate(uint32_t TickRate){
  Task::pTask_Manual_TickRate = TickRate;
}
void                    Task::API_Set_State(uint8_t State){
  Task::pTask_state = (Task_state_Typedef)pTask_state;
}
void                    Task::API_Set_Priority(uint32_t NewPriority){
  vTaskPrioritySet (API_Use_Ptr(Task::pTask_handle), NewPriority);
}


/* Definitions of private methods --------------------------------------------*/

/* Definitions of task body --------------------------------------------------*/

void Task::Task_body(void * argument){  //���� ����� �� ����������������!
  
  API_CPU_Set_OC_IDLE_DO(0);
  
  //���������� �������������  
  if (Task_body_Ptr == 0) this->Task_body_init();
  
  //����������� ����� ����� �����
  this->pTask_DelayUntil_Latch = xTaskGetTickCount();
  
  for(;;)
  {
    //�������� ��� ������������ ����
    API_CPU_Set_OC_IDLE_DO(0);
    
    //������������� �������� ������
    this->pTask_state = TASK_DOWORK;
    
    //�������� ����������� �������� ������. ������ FPS �����.
    if ( API_Check_RAM_Ptr(pTask_Glob_cnt_ptr))// != 0 ) 
    {//���� ������� ������
      //�������� �������
      uint32_t CarrentTic = *pTask_Glob_cnt_ptr;
      //uint32_t CarrentTic = xTaskGetTickCount();
      //���� ������� ���������� �� ���. �������� �����  ��� �� configTICK_RATE_HZ ������� ������
      uint32_t Tick_Rate = ((pTask_Manual_TickRate == 0)? configTICK_RATE_HZ: pTask_Manual_TickRate);
      
      if ( CarrentTic - this->pTask_Glob_cnt_Lach >= Tick_Rate )
      {
        this->pTask_Glob_cnt_Lach = CarrentTic;
        this->pTask_FPS = this->pTask_Glob_cnt + 1;
        this->pTask_Glob_cnt = 0;
      }
      else
      {
        this->pTask_Glob_cnt++;
      }
    }
    
    //��������� �����
    if (Task_body_Ptr == 0)this->Task_body_process();
    else Task_body_Ptr(this);
    
    //���� ��������� ��������� �����
    if ( this->pTask_priority != this->API_Get_Priority()){
      this->API_Set_Priority(this->pTask_priority);
    }
    //���� pTask_WkUp_Laten > 0 
    if (pTask_WkUp_Laten > 0 ){ //������� ������� �����
      
      //���� ������ ���� ���������
      if (pTask_WkUp_Laten != pTask_WkUp_Laten_Prv)
      {
        pTask_DelayUntil_Latch = pTask_Glob_cnt_Lach;
        pTask_WkUp_Laten_Prv = pTask_WkUp_Laten;
      }
      
      //�������� �������
      uint32_t CarrentTic = *pTask_Glob_cnt_ptr;
      if ( (CarrentTic - pTask_DelayUntil_Latch ) > (pTask_WkUp_Laten * 2))
      {
        pTask_DelayUntil_Latch = (CarrentTic - pTask_WkUp_Laten);
      }
      //���
      vTaskDelayUntil( &this->pTask_DelayUntil_Latch, pTask_WkUp_Laten );
    }
    
    //��������� ��������� ������ �����
    this->pTask_heap_WaterMark = 4*(uint16_t)uxTaskGetStackHighWaterMark(this->pTask_handle);
     
    //���������� ���������� ������������ �����
    if ( this->pTask_state == TASK_STACK_FAIL ){
      for(;;);
    }
    //���������� ���������� ��������� ��������� �����
    if ( this->pTask_state == TASK_HEADR_FAIL ){
      for(;;);
    }    
  }    
}

/* Definitions of task costructor/destructor ---------------------------------*/



Task::Task(void (*Task_body)(void *),char const* TaskName, uint16_t Need_heap):BaseClass(sizeof (Task)){
  //������ ���������  
  this->Task_body_Ptr            = Task_body;
  this->pTask_Glob_cnt_Lach      = 0;
  this->pTask_Glob_cnt           = 0;
  this->pTask_User_cnt_Lach      = 0;
  this->pTask_WkUp_Laten         = 100;
  
  //���������� ����������
  this->pTask_Manual_TickRate    = 0;
  this->pTask_FPS                = 0;
  this->pTask_Glob_cnt_ptr       = 0;
  this->pTask_handle             = 0;
  this->pTask_state              = TASK_NOT_CREATE;
  this->pTask_priority           = 3;
  this->pTask_total_heap         = 0;
  this->pTask_user_heap          = 0;
  this->pTask_name               = TaskName;
  
  //������ ���� � U32. � ������� 19 U32 ����� 
  uint32_t Task_Heap = (Need_heap/4) + 19; 
  
  //��������������� ��������� �� ���� �����
  void (Task::*MethodName)(void *) = &Task::Task_body;
  pdTASK_CODE* proc = ((pdTASK_CODE*)(&MethodName));
  
  //����� ����
  xTaskCreate          (  *proc,\
                       (char const*) TaskName,\
                       Task_Heap,\
                       this,\
                       this->pTask_priority,\
                       &pTask_handle);
  
  //��������� �������� ����?
  if (API_Check_RAM_Ptr(this->pTask_handle)) 
  {
    //���� ������� ��������
    this->pTask_state = TASK_STARTED;
  
    tskTCB* pTask_handle_Ext = (tskTCB*)pTask_handle;
  
    this->pTask_total_heap += (sizeof (tskTCB));
    
    this->pTask_total_heap += (uint32_t)pTask_handle_Ext->pxTopOfStack - (uint32_t)pTask_handle_Ext;
    
    this->pTask_user_heap  += (uint32_t)pTask_handle_Ext->pxTopOfStack - (uint32_t)pTask_handle_Ext->pxStack;
  }
  else
  {//���� �� ��������
    this->pTask_state = TASK_NOT_CREATE;
    this->~Task();
  }
}
Task::Task(char const* TaskName, uint16_t Need_heap):BaseClass(sizeof (Task)){
  //������ ���������  
  this->Task_body_Ptr            = 0;
  this->pTask_Glob_cnt_Lach      = 0;
  this->pTask_Glob_cnt           = 0;
  this->pTask_User_cnt_Lach      = 0;
  this->pTask_WkUp_Laten         = 100;
  
  //���������� ����������
  this->pTask_Manual_TickRate    = 0;
  this->pTask_FPS                = 0;
  this->pTask_Glob_cnt_ptr       = 0;
  this->pTask_handle             = 0;
  this->pTask_state              = TASK_NOT_CREATE;
  this->pTask_priority           = 3;
  this->pTask_total_heap         = 0;
  this->pTask_user_heap          = 0;
  this->pTask_name               = TaskName;
  
  //������ ���� � U32. � ������� 19 U32 ����� 
  uint32_t Task_Heap = (Need_heap/4) + 19; 
  
  //��������������� ��������� �� ���� �����
  void (Task::*MethodName)(void *) = &Task::Task_body;
  pdTASK_CODE* proc = ((pdTASK_CODE*)(&MethodName));  
  
  //����� ����
  xTaskCreate          (  *proc,\
                       (char const*) TaskName,\
                       Task_Heap,\
                       this,\
                       this->pTask_priority,\
                       &pTask_handle);
  
  //��������� �������� ����?
  if (API_Check_RAM_Ptr(this->pTask_handle)) 
  {
    //���� ������� ��������
    this->pTask_state = TASK_STARTED;
  
    tskTCB* pTask_handle_Ext = (tskTCB*)pTask_handle;
  
    this->pTask_total_heap += (sizeof (tskTCB));
    
    this->pTask_total_heap += (uint32_t)pTask_handle_Ext->pxTopOfStack - (uint32_t)pTask_handle_Ext;
    
    this->pTask_user_heap  += (uint32_t)pTask_handle_Ext->pxTopOfStack - (uint32_t)pTask_handle_Ext->pxStack;
  }
  else
  {//���� �� ��������
    this->pTask_state = TASK_NOT_CREATE;
    this->~Task();
  }
}
Task::~Task(){
  //�������� ��������������� ���������������
  this->Task_body_deinit();
  
  //��������� ����� ���� �� ��� ������
  if (this->pTask_state != TASK_NOT_CREATE) vTaskDelete(this->pTask_handle);
}

/*************************** (C) __________ LLC ************** end of file ****/