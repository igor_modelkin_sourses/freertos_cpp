/**************************** (C) __________ LLC *******************************
 * @module  SYS
 * @file    SYS.h
 * @version 1.0.0
 * @date    XX.XX.XXXX
 * $brief   Interface of the MODULE module.
 *          [...]
 *******************************************************************************
 * @history     Version  Author      Comment
 * XX.XX.XXXX   1.0.0    XXX         First release.
 *******************************************************************************
 */

/* Define to prevent recursive  ----------------------------------------------*/
#ifndef QUEUE_T_H
#define QUEUE_T_H



/* Includes ------------------------------------------------------------------*/

#include "stdint.h"
#include "cmsis_os.h"
//#include "FreeRTOS.h"
//#include "queue.h"

/* Definitions of global (public) constants ----------------------------------*/

/* Declarations of global (public) data types --------------------------------*/

/* Declarations of global (public) class -------------------------------------*/

class Queue 
{  
public:
  /* Declarations of public class variables ----------------------------------*/  
  
  /* Declarations of public class methods ------------------------------------*/  
  uint16_t      API_Get_Total_heap      (void);
  uint16_t      API_Get_Mess_Free       (void); //����� ��������� ���������
  uint16_t      API_Get_Mess_Cnt        (void); //����� ��������� � �������
  uint16_t      API_Get_Mess_Nrb        (void); //������ �������
  uint16_t      API_Get_Mess_Size       (void); //������ ���������
  
  uint16_t      API_MessWait            (void);  
  uint16_t      API_Receive             (void * pvBuffer, uint32_t xTicksToWait);
  uint16_t      API_Peek                (void * pvBuffer, uint32_t xTicksToWait);
  uint16_t      API_SendToBack          (void * pvBuffer, uint32_t xTicksToWait);//FIFO
  uint16_t      API_SendToFront         (void * pvBuffer, uint32_t xTicksToWait);//LIFO
  uint16_t      API_Overwrite           (void * pvBuffer);
  
  
  uint16_t      API_Get_Mess_FreeISR    (void); //����� ��������� ���������
  uint16_t      API_Get_Mess_CntISR     (void); //����� ��������� � �������
  uint16_t      API_Get_Mess_NrbISR     (void); //������ �������
  uint16_t      API_Get_Mess_SizeISR    (void); //������ ���������
  
  uint16_t      API_MessWaitISR         (void);
  uint16_t      API_ReceiveISR          (void * pvBuffer, uint32_t ForceTaskYIELD);  
  uint16_t      API_PeekISR             (void * pvBuffer);
  uint16_t      API_SendToBackISR       (void * pvBuffer, uint32_t ForceTaskYIELD);//FIFO
  uint16_t      API_SendToFrontISR      (void * pvBuffer, uint32_t ForceTaskYIELD);//LIFO
  uint16_t      API_OverwriteISR        (void * pvBuffer, uint32_t ForceTaskYIELD);
  
  
  
protected:
  /* Declarations of protected class variables -------------------------------*/ 
  xQueueHandle  pQueue_handle;
  uint16_t      pQueue_total_heap;
  uint16_t      pQueue_Length;
  uint16_t      pQueue_Size;
  
private:
  /* Declarations of private class variables ---------------------------------*/  

  
  /* Declarations of private methods -----------------------------------------*/
  
public:
  /* Declarations of task costructor/destructor ------------------------------*/
  void* operator new    (size_t size);
  void  operator delete (void* Ptr);
  
  Queue(uint16_t Length, uint16_t Size);  
  ~Queue();
};

#endif  /* #ifndef MODULE_H */

/*************************** (C) __________ LLC ************** end of file ****/
